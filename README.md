# ASE-2021-Y1S2-ICS-7

This is the repository of team 7 for the ICS client project.


----- TO RUN THE SERVER -----

Clone this repository
Open CMD at repository route

Type the following commands (Windows):

pipenv shell

pipenv install flask
pipenv install flask-session
pipenv install mysql-connector-python
pipenv install bcrypt

python server.py

The following login details can be used to access the system:

Role: Technician
Email: sampleEmail1@email.com
Password: 12345

Role: Staff
Email: sampleEmail2@email.com
Password: 12345

Role: Student
Email: sampleEmail3@email.com
Password: 12345


----- RULES OF ENGAGEMENT -----

- Always ensure stories assigned to them have the correct status on the issue board.
- Attend organised meetings to discuss merge requests or project progress.
- Follow the feature branching flow discussed in the initial meeting.
- Do NOT push to the master branch without the consent of the rest of the team.
- Ensure that only working code is pushed to the develop branch.
- Respond to testing requests from other team members within 48 working hours.
- Do NOT work on stories assigned to another team member without prior consent.
- Be honest, raise issues if they arise, help others and share knowledge.

----- ADD AND EDITING PAGES -----

Jinja templating has been setup to utilise inheritance, reducing code duplication
and improving readability. You should stick to the following when editing pages:
when editing pages:

Body content should be added to {% block body %}
Script should be added to {% block script %}

ALWAYS USE {{ super() }} TO PREVENT OVERRIDING A PARENT TEMPLATE

To create a new page:
New pages should use {% extends "standardPage.html" %} to inherit the header, footer, navbar and styling.
If you want to inherit the styling but nothing else, use {% extends "base.html" %}

To link external files (jQuery, CSS, etc):
Go to the base.html file. Add the link in the head section. This will be inherited by every page.
