SET FOREIGN_KEY_CHECKS=0;

-- -----------------------------------------------------
-- Schema ics
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `ics` ;

-- -----------------------------------------------------
-- Schema ics
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ics` DEFAULT CHARACTER SET utf8 ;
USE `ics` ;

-- -----------------------------------------------------
-- Table `ics`.`accessLevel`
--
-- Stores system permission levels for users
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ics`.`accessLevel` ;

CREATE TABLE IF NOT EXISTS `ics`.`accessLevel` (
  `accessLevelID` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`accessLevelID`));


-- -----------------------------------------------------
-- Table `ics`.`users`
--
-- Stores all system users personal details and access level
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ics`.`users` ;

CREATE TABLE IF NOT EXISTS `ics`.`users` (
  `userID` INT NOT NULL AUTO_INCREMENT,
  `firstName` VARCHAR(45) NOT NULL,
  `lastName` VARCHAR(45) NOT NULL,
  `emailAddress` VARCHAR(45) NOT NULL,
  `password` VARCHAR(200) NOT NULL,
  `accessLevelID` INT NOT NULL,
  `associatedStaff` INT NULL,
  `contactNumber` VARCHAR(12) NOT NULL,
  PRIMARY KEY (`userID`),
  CONSTRAINT `accessLevelID` FOREIGN KEY (`accessLevelID`) REFERENCES `ics`.`accessLevel` (`accessLevelID`),
  CONSTRAINT `userID` FOREIGN KEY (`associatedStaff`) REFERENCES `ics`.`users` (`userID`));


-- -----------------------------------------------------
-- Table `ics`.`equipmentCategories`
--
-- Allows equipment to be categorised for filtering purposes
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ics`.`equipmentCategories` ;

CREATE TABLE IF NOT EXISTS `ics`.`equipmentCategories` (
  `equipmentCategoryID` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`equipmentCategoryID`));


-- -----------------------------------------------------
-- Table `ics`.`equipment`
--
-- Stores all equipment stock for booking requests 
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ics`.`equipment` ;

CREATE TABLE IF NOT EXISTS `ics`.`equipment` (
  `equipmentID` INT NOT NULL AUTO_INCREMENT,
  `model` VARCHAR(45) NOT NULL,
  `equipmentCategoryID` INT NOT NULL,
  `location` VARCHAR(45) NOT NULL,
  `servicePeriod` VARCHAR(45) NOT NULL,
  `lastReset` datetime default current_timestamp,
  PRIMARY KEY (`equipmentID`),
  CONSTRAINT `equipmentCategoryID` FOREIGN KEY (`equipmentCategoryID`) REFERENCES `ics`.`equipmentCategories` (`equipmentCategoryID`))
;


-- -----------------------------------------------------
-- Table `ics`.`bookings`
--
-- Stores all booking requests made be users
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ics`.`bookings` ;

CREATE TABLE IF NOT EXISTS `ics`.`bookings` (
  `bookingID` INT NOT NULL AUTO_INCREMENT,
  `userID` INT NOT NULL,
  `bookedStartTime` DATETIME NOT NULL,
  `bookedFinishTime` DATETIME NOT NULL,
  `realStartTime` DATETIME NULL,
  `realFinishTime` DATETIME NULL,
  `verified` TINYINT(1) NOT NULL DEFAULT 0,
  
  -- -----------------------------------------------------
  -- Verified: Values
  -- 0 - Awaiting Moderation
  -- 1 - Approved
  -- 2 - Denied
  -- 3 - Cancelled

-- -----------------------------------------------------
  
  `notes` VARCHAR(200),
  PRIMARY KEY (`bookingID`),
  CONSTRAINT `userID1` FOREIGN KEY (`userID`) REFERENCES `ics`.`users` (`userID`) ON DELETE CASCADE)
;


-- -----------------------------------------------------
-- Table `ics`.`training`
--
-- Stores all training modules required to use equipment
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ics`.`training` ;

CREATE TABLE IF NOT EXISTS `ics`.`training` (
  `trainingID` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`trainingID`))
  ;


-- -----------------------------------------------------
-- Table `ics`.`equipmentTraining`
-- 
-- Assigns training modules to equipment
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ics`.`equipmentTraining` ;

CREATE TABLE IF NOT EXISTS `ics`.`equipmentTraining` (
  `equipmentTrainingID` INT NOT NULL AUTO_INCREMENT,
  `trainingID` INT NOT NULL,
  `equipmentID` INT NOT NULL,
  PRIMARY KEY (`equipmentTrainingID`),
  CONSTRAINT `trainingID` FOREIGN KEY (`trainingID`) REFERENCES `ics`.`training` (`trainingID`),
  CONSTRAINT `equipmentID` FOREIGN KEY (`equipmentID`) REFERENCES `ics`.`equipment` (`equipmentID`))
;


-- -----------------------------------------------------
-- Table `ics`.`userTraining`
--
-- Stores which training modules each user possess
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ics`.`userTraining` ;

CREATE TABLE IF NOT EXISTS `ics`.`userTraining` (
  `userTrainingID` INT NOT NULL AUTO_INCREMENT,
  `userID` INT NOT NULL,
  `trainingID` INT NOT NULL,
  PRIMARY KEY (`userTrainingID`),
  CONSTRAINT `userID2` FOREIGN KEY (`userID`) REFERENCES `ics`.`users` (`userID`),
  CONSTRAINT `trainingID1` FOREIGN KEY (`trainingID`) REFERENCES `ics`.`training` (`trainingID`))
;


-- -----------------------------------------------------
-- Table `ics`.`bookedEquipment`
--
-- Stores which equipment has been requested for each booking
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ics`.`bookedEquipment` ;

CREATE TABLE IF NOT EXISTS `ics`.`bookedEquipment` (
  `bookedEquipmentID` INT NOT NULL AUTO_INCREMENT,
  `bookingID` INT NOT NULL,
  `equipmentID` INT NOT NULL,
  PRIMARY KEY (`bookedEquipmentID`),
  CONSTRAINT `equipmentID1` FOREIGN KEY (`equipmentID`) REFERENCES `ics`.`equipment` (`equipmentID`),
  CONSTRAINT `bookingID` FOREIGN KEY (`bookingID`) REFERENCES `ics`.`bookings` (`bookingID`) ON DELETE CASCADE)
;


-- -----------------------------------------------------
-- Table `ics`.`chemicals`
--
-- Stores all chemicals bookable by users
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ics`.`chemicals` ;

CREATE TABLE IF NOT EXISTS `ics`.`chemicals` (
  `chemicalID` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `quantity` INT NOT NULL,
  `deliveryDate` DATE NOT NULL,
  `expiryDate` DATE NOT NULL,
  PRIMARY KEY (`chemicalID`))
;


-- -----------------------------------------------------
-- Table `ics`.`bookedChemicals`
--
-- Stores which chemicals have been requested for each booking
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ics`.`bookedChemicals` ;

CREATE TABLE IF NOT EXISTS `ics`.`bookedChemicals` (
  `bookedChemicalsID` INT NOT NULL AUTO_INCREMENT,
  `bookingID` INT NOT NULL,
  `chemicalID` INT NOT NULL,
  PRIMARY KEY (`bookedChemicalsID`),
  CONSTRAINT `bookingID1` FOREIGN KEY (`bookingID`) REFERENCES `ics`.`bookings` (`bookingID`) ON DELETE CASCADE,
  CONSTRAINT `chemicalID` FOREIGN KEY (`chemicalID`) REFERENCES `ics`.`chemicals` (`chemicalID`))
;


-- -----------------------------------------------------
-- Table `ics`.`equipmentServices`
--
-- Stores the service history of equipment
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ics`.`equipmentServices` ;

CREATE TABLE IF NOT EXISTS `ics`.`equipmentServices` (
  `equipmentServiceID` INT NOT NULL AUTO_INCREMENT,
  `equipmentID` INT NOT NULL,
  `date` DATE NOT NULL,
  `engineer` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`equipmentServiceID`),
  CONSTRAINT `equipmentID2` FOREIGN KEY (`equipmentID`) REFERENCES `ics`.`equipment` (`equipmentID`))
;


-- -----------------------------------------------------
-- Table `ics`.`gases`
--
-- Stores all gases bookable by users
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ics`.`gases` ;

CREATE TABLE IF NOT EXISTS `ics`.`gases` (
  `gasID` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `deliveryDate` DATE NOT NULL,
  `expiryDate` DATE NOT NULL,
  `quantity` INT NOT NULL,
  PRIMARY KEY (`gasID`))
;


-- -----------------------------------------------------
-- Table `ics`.`bookedGases`
--
-- Stores which gases have been requested for each booking
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ics`.`bookedGases` ;

CREATE TABLE IF NOT EXISTS `ics`.`bookedGases` (
  `bookedGasesID` INT NOT NULL AUTO_INCREMENT,
  `bookingID` INT NOT NULL,
  `gasID` INT NOT NULL,
  PRIMARY KEY (`bookedGasesID`),
  CONSTRAINT `bookingID0` FOREIGN KEY (`bookingID`) REFERENCES `ics`.`bookings` (`bookingID`) ON DELETE CASCADE,
  CONSTRAINT `gasID1` FOREIGN KEY (`gasID`) REFERENCES `ics`.`gases` (`gasID`))
;


-- -----------------------------------------------------
-- Table `ics`.`navOptions`
--
-- Stores all navigation options for all permission levels
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ics`.`navOptions` ;

CREATE TABLE IF NOT EXISTS `ics`.`navOptions` (
  `navOptionID` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `iconFile` VARCHAR(45) NOT NULL,
  `link` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`navOptionID`));
  
  -- -----------------------------------------------------
-- Table `ics`.`navRelations`
--
-- Stores which permission levels have access to which navigation options
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ics`.`navRelations` ;

CREATE TABLE IF NOT EXISTS `ics`.`navRelations` (
  `navRelationID` INT NOT NULL AUTO_INCREMENT,
  `navOptionID` INT NOT NULL,
  `accessLevelID` INT NOT NULL,
  CONSTRAINT `accessLevelID1` FOREIGN KEY (`accessLevelID`) REFERENCES `ics`.`accessLevel` (`accessLevelID`),
  CONSTRAINT `navOptionID` FOREIGN KEY (`navOptionID`) REFERENCES `ics`.`navOptions` (`navOptionID`),
  PRIMARY KEY (`navRelationID`));
  
  
-- -----------------------------------------------------
-- Table `ics`.`logs`
--
-- Stores manual and automatic logs when important changes are made to the database
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ics`.`logs` ;

CREATE TABLE IF NOT EXISTS `ics`.`logs` (
  `logID` INT NOT NULL AUTO_INCREMENT,
  `userID` INT NOT NULL,
  `event` VARCHAR(150) NOT NULL,
  `timestamp` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT `userID3` FOREIGN KEY (`userID`) REFERENCES `ics`.`users` (`userID`),
  PRIMARY KEY (`logID`));
  
    -- -----------------------------------------------------
-- View: equipmentView
--
-- Simplifies equipment information
-- -----------------------------------------------------

CREATE VIEW equipmentView AS
SELECT e.equipmentID, CONCAT(e.model," ", ec.name) as name, e.location, e.servicePeriod, e.model, ec.name as category, e.lastReset
FROM equipment e
INNER JOIN equipmentCategories ec
ON e.equipmentCategoryID = ec.equipmentCategoryID;
  

  -- -----------------------------------------------------
-- View: extraBookingDetails
-- -----------------------------------------------------

CREATE VIEW extraBookingDetails AS
SELECT b.bookingID, b.userID, u.firstName, u.lastName, t.name FROM bookings b
LEFT JOIN usertraining ut ON b.userID = ut.userID
LEFT JOIN training t ON ut.trainingID = t.trainingID
LEFT JOIN users u on b.userID = u.userID;

  -- -----------------------------------------------------
-- View: accessLevelNavOptions
--
-- Shows the navigation options for each user type
-- -----------------------------------------------------

CREATE VIEW accessLevelNavOptions AS
SELECT nr.navRelationID, nr.navOptionID, no.name as navOptionName, no.iconFile, no.link, nr.accessLevelID, al.name as accessLevelName
FROM navRelations nr
INNER JOIN navOptions no
ON no.navOptionID = nr.navOptionID
INNER JOIN accessLevel al
ON al.accessLevelID = nr.accessLevelID
ORDER BY nr.navRelationID;

  -- -----------------------------------------------------
-- View: totalBookingsView
--
-- Simplifies querying all booked equipment
-- -----------------------------------------------------

CREATE VIEW totalBookingsView AS
SELECT b.bookingID, e.equipmentID, e.name, b.realStartTime, b.realFinishTime, e.lastReset FROM bookings b
LEFT JOIN bookedequipment be ON be.bookingID=b.bookingID
LEFT JOIN equipmentView e ON be.equipmentID=e.equipmentID;

  -- -----------------------------------------------------
-- View: userTrainingView
--
-- Displays some normalised data about user's training
-- -----------------------------------------------------

CREATE VIEW userTrainingView AS
SELECT u.userID, u.firstName, u.lastName,  t.name FROM users u
LEFT JOIN usertraining ut ON u.userID = ut.userID
LEFT JOIN training t ON ut.trainingID = t.trainingID
ORDER BY u.lastName;


  -- -----------------------------------------------------
-- View: bookingView
--
-- Simplifies booking information
-- -----------------------------------------------------

CREATE VIEW bookingView AS
SELECT bookingID,
DATE_FORMAT(bookedStartTime, '%d/%m/%Y') as date,
DATE_FORMAT(bookedStartTime, '%H:%i') as startTime,
DATE_FORMAT(bookedFinishTime, '%H:%i') as finishTime,
verified,
userID
FROM bookings;

  -- -----------------------------------------------------
-- View: bookedEquipmentView
--
-- Shows which items of equipment are associated with which booking
-- -----------------------------------------------------

CREATE VIEW bookedEquipmentView AS
SELECT be.bookedEquipmentID, be.bookingID, be.equipmentID, e.name AS equipmentName, b.userID
FROM bookedEquipment be
INNER JOIN equipmentView e
ON e.equipmentID = be.equipmentID
INNER JOIN bookings b
ON b.bookingID = be.bookingID;

  -- -----------------------------------------------------
-- View: bookedChemicalView
--
-- Shows which chemiclas are associated with which booking
-- -----------------------------------------------------

CREATE VIEW bookedChemicalView AS
SELECT bc.bookedChemicalsID, bc.bookingID, bc.chemicalID, c.name AS chemicalName, b.userID
FROM bookedChemicals bc
INNER JOIN chemicals c
ON c.chemicalID = bc.chemicalID
INNER JOIN bookings b
ON b.bookingID = bc.bookingID;

  -- -----------------------------------------------------
-- View: bookedGasView
--
-- Shows which gases are associated with which booking
-- -----------------------------------------------------

CREATE VIEW bookedGasView AS
SELECT bg.bookedGasesID, bg.bookingID, bg.gasID, g.name AS gasName, b.userID
FROM bookedGases bg
INNER JOIN gases g
ON g.gasID = bg.gasID
INNER JOIN bookings b
ON b.bookingID = bg.bookingID;

  -- -----------------------------------------------------
-- View: todaysBookingsSummaryView
--
-- Shows all the bookings for the today
-- -----------------------------------------------------

CREATE VIEW todaysBookingsSummaryView AS
SELECT e.name, u.firstName, u.LastName, DATE_FORMAT(b.bookedStartTime, "%H:%i") as startTime
FROM bookedEquipment be
INNER JOIN bookings b
ON b.bookingID = be.bookingID
INNER JOIN equipmentView e
ON e.equipmentID = be.equipmentID
INNER JOIN users u
ON u.userID = b.userID
WHERE DATE(b.bookedStartTime) = DATE(NOW()) AND b.verified = 1
ORDER BY startTime;


  -- -----------------------------------------------------
-- View: requiredTrainingForEquipmentByuser
--
-- Shows which training modules require completing to operate each piece of equipment
-- -----------------------------------------------------

CREATE VIEW requiredTrainingForEquipmentByUser AS
SELECT e.equipmentID, t.trainingID, t.name, u.userId
FROM equipmentView e, training t, equipmenttraining et, users u
WHERE e.equipmentID = et.equipmentID
AND et.trainingID = t.trainingID
AND (et.trainingID, u.userID) NOT IN (select trainingID, userID FROM usertraining)
ORDER BY u.userID;

  -- -----------------------------------------------------
-- View: userView
--
-- Shows all users and their access levels
-- -----------------------------------------------------

CREATE VIEW userView AS
SELECT u.userID, u.firstName, u.lastName, u.emailAddress, u.contactNumber, (SELECT name FROM accessLevel WHERE accessLevelID = u.accessLevelID) as accessLevel
FROM users u;


  -- -----------------------------------------------------
-- View: equipmentTrainingView
--
-- Shows training for all equipment
-- -----------------------------------------------------

CREATE VIEW equipmentTrainingView AS
SELECT et.trainingID, t.name, et.equipmentID
FROM equipmentTraining et
INNER JOIN training t
ON t.trainingID = et.trainingID;



  -- -----------------------------------------------------
-- View: logView
--
-- Shows the system logs with a users name
-- -----------------------------------------------------

CREATE VIEW logView AS
SELECT CONCAT(u.firstName," ", u.lastName) as userName, l.event, l.timestamp
FROM logs l
INNER JOIN users u
ON u.userID = l.userID
ORDER BY l.timestamp;

-- -----------------------------------------------------
-- Procedure: searchEquipment
--
-- Returns all equipment where the name is similar to the search parameter
-- -----------------------------------------------------

DELIMITER //
CREATE PROCEDURE searchEquipment
(IN search CHAR(30))
BEGIN
  SET @search = search; 
  SELECT * from equipmentView
  WHERE name LIKE CONCAT('%', @search, '%');
END //
DELIMITER ;


-- -----------------------------------------------------
-- Procedure: addLog
--
-- Adds a log to the log table
-- -----------------------------------------------------

DELIMITER //
CREATE PROCEDURE addLog
(IN givenUserID INT, IN givenEvent VARCHAR(150))
BEGIN
  INSERT INTO logs (userID, event) VALUES (givenUserID, givenEvent);
END //
DELIMITER ;

  -- -----------------------------------------------------
-- Procedure: searchGases
--
-- Returns all gases where the name is similar to the search parameter
-- -----------------------------------------------------

DELIMITER //
CREATE PROCEDURE searchGases
(IN search CHAR(30))
BEGIN
  SET @search = search; 
  SELECT gasID, name from gases
  WHERE name LIKE CONCAT('%', @search, '%');
END //
DELIMITER ;

  -- -----------------------------------------------------
-- Procedure: searchChemicals
--
-- Returns all chemicals where the name is similar to the search parameter
-- -----------------------------------------------------

DELIMITER //
CREATE PROCEDURE searchChemicals
(IN search CHAR(30))
BEGIN
  SET @search = search; 
  SELECT chemicalID, name from chemicals
  WHERE name LIKE CONCAT('%', @search, '%');
END //
DELIMITER ;

-- -----------------------------------------------------
-- Procedure: createBooking
--
-- Creates a booking request for a user
-- -------------------------------------------------------

DELIMITER //
CREATE PROCEDURE createBooking(IN givenUserID int, IN givenBookedStartDate datetime, IN givenBookedFinishDate datetime, IN givenNotes VARCHAR(200))
BEGIN

	DECLARE EXIT HANDLER FOR 1452
	BEGIN
		SELECT 'The booking could not be created due to it being associated with an invalid user. Please try again.';
		ROLLBACK;
	END;

START TRANSACTION;
	INSERT INTO bookings (userID, bookedStartTime, bookedFinishTime, notes) VALUES (givenUserID, givenBookedStartDate, givenBookedFinishDate, givenNotes);
	SELECT 'success', LAST_INSERT_ID();
COMMIT;
END //
DELIMITER ;


-- -----------------------------------------------------
-- Procedure: deleteBooking
--
-- Deletes a booking from the database. Only used in critical failiure by the system when other rollback methods have failed.
-- Cancelled bookings by users are given a status of 4
-- -------------------------------------------------------

DELIMITER //
CREATE PROCEDURE deleteBooking(IN givenBookingID int)
BEGIN
START TRANSACTION;
	IF (bookingExists(givenBookingID) = 0) THEN
		SELECT 'There is no booking assoicated with the ID provided';
    ELSE
		DELETE FROM bookings WHERE bookingID = givenBookingID;
		SELECT 'success';
	END IF;
COMMIT;
END //
DELIMITER ;


-- -----------------------------------------------------
-- Procedure: addEquipmentToBooking
--
-- Adds an item of equipment to a booking request
-- -------------------------------------------------------

DELIMITER //
CREATE PROCEDURE addEquipmentToBooking(givenBookingID int, givenEquipmentID int)
BEGIN
	START TRANSACTION;
    
    IF (bookingExists(givenBookingID) = 1) THEN
        INSERT INTO bookedEquipment (bookingID, equipmentID) VALUES (givenBookingID, givenEquipmentID);
        SELECT 'success';
        COMMIT;
	ELSE
		ROLLBACK;
        SELECT 'There is no booking assoicated with the ID provided';
	END IF;
    
END //
DELIMITER ;


-- -----------------------------------------------------
-- Procedure: addChemicalToBooking
--
-- Adds a chemical to a booking request
-- -------------------------------------------------------

DELIMITER //
CREATE PROCEDURE addChemicalToBooking(givenBookingID int, givenChemicalID int)
BEGIN
	START TRANSACTION;
    
    IF (bookingExists(givenBookingID) = 1) THEN
        INSERT INTO bookedChemicals (bookingID, chemicalID) VALUES (givenBookingID, givenChemicalID);
        SELECT 'success';
        COMMIT;
	ELSE
		ROLLBACK;
        SELECT 'There is no booking assoicated with the ID provided';
	END IF;
    
END //
DELIMITER ;

-- -----------------------------------------------------
-- Procedure: addGasToBooking
--
-- Adds a gas to a booking request
-- -------------------------------------------------------

DELIMITER //
CREATE PROCEDURE addGasToBooking(givenBookingID int, givenGasID int)
BEGIN
	START TRANSACTION;
    
    IF (bookingExists(givenBookingID) = 1) THEN
        INSERT INTO bookedGases (bookingID, gasID) VALUES (givenBookingID, givenGasID);
        SELECT 'success';
        COMMIT;
	ELSE
		ROLLBACK;
        SELECT 'There is no booking assoicated with the ID provided';
	END IF;
    
END //
DELIMITER ;

  -- -----------------------------------------------------
-- Procedure: getUnavailableEquipment
--
-- Returns equipment that is unavailable for the specified times due to it already being booked
--
-- IMPORTANT!
-- Do NOT use BETWEEN for this DATETIME comparison. It prevents bookings of equipment
-- being made directly after a previous booking, even though the equipment is available.
-- -----------------------------------------------------

DELIMITER //
CREATE PROCEDURE getUnavailableEquipment
(IN startTime DATETIME, IN finishTime DATETIME)
BEGIN
	SELECT be.equipmentID
	FROM bookedEquipment be
	INNER JOIN bookings b
	ON b.bookingID = be.bookingID
	WHERE (b.bookedStartTime > startTime AND bookedStartTime <= finishTime)
    OR (b.bookedFinishTime > startTime AND bookedFinishTime <= finishTime);
END //
DELIMITER ;

-- -----------------------------------------------------
-- Procedure: getEquipmentTimes
--
-- Works out the total equipment time since last reset (0) or returns same data for all other equipment
-- -------------------------------------------------------

DELIMITER //
CREATE PROCEDURE getEquipmentTimes(IN allInfo int)
BEGIN
	IF(allInfo = 0) THEN
		SELECT name, sum(time_to_sec(timediff(realFinishTime, realStartTime))) /3600 as "Total hours" FROM totalBookingsView
		WHERE realStartTime >= lastReset GROUP BY name;
	ELSE
		SELECT name, NULL as "Total hours" FROM totalBookingsView
		WHERE realStartTime is NULL OR realStartTime <lastReset;
	END IF;
    
END //
DELIMITER ;

-- -----------------------------------------------------
-- Function: bookingExists
--
-- Returns True/False if a booking with the provided bookingID exists
-- -------------------------------------------------------

DELIMITER //

CREATE FUNCTION bookingExists(givenBookingID int)
RETURNS INT
DETERMINISTIC
BEGIN

IF (SELECT COUNT(*) FROM bookings WHERE bookingID = givenBookingID) > 0 THEN
	RETURN 1;
ELSE
	RETURN 0;
END IF;

END //

DELIMITER ;

-- -----------------------------------------------------
-- Trigger: logNewbooking
--
-- Adds a log when a new booking is added to the bookings table
-- -----------------------------------------------------

DELIMITER //
DROP TRIGGER IF EXISTS `ics`.`logNewBooking` //

CREATE TRIGGER logNewBooking
AFTER INSERT ON `bookings` FOR EACH ROW
BEGIN 
    CALL addLog(NEW.userID, CONCAT("Booking ",NEW.bookingID," added to system"));
END //
DELIMITER ; 


-- -----------------------------------------------------
-- Trigger: logDeletebooking
--
-- Adds a log when a booking is deleted from the bookings table
-- -----------------------------------------------------

DELIMITER //
DROP TRIGGER IF EXISTS `ics`.`logDeleteBooking` //

CREATE TRIGGER logDeleteBooking
BEFORE DELETE ON `bookings` FOR EACH ROW
BEGIN 
    CALL addLog(OLD.userID, CONCAT("Booking ",OLD.bookingID," deleted from system"));
END //
DELIMITER ; 

-- -----------------------------------------------------
-- Sample data: accessLevel
-- -----------------------------------------------------

INSERT INTO accessLevel (name) VALUES ("Technician");
INSERT INTO accessLevel (name) VALUES ("Staff");
INSERT INTO accessLevel (name) VALUES ("Student");

-- -----------------------------------------------------
-- Sample data: bookedChemicals
-- -----------------------------------------------------

INSERT INTO bookedChemicals (bookingID, chemicalID) VALUES (0,0);
INSERT INTO bookedChemicals (bookingID, chemicalID) VALUES (4,1);
INSERT INTO bookedChemicals (bookingID, chemicalID) VALUES (1,2);
INSERT INTO bookedChemicals (bookingID, chemicalID) VALUES (2,3);
INSERT INTO bookedChemicals (bookingID, chemicalID) VALUES (3,4);
INSERT INTO bookedChemicals (bookingID, chemicalID) VALUES (7,2);


-- -----------------------------------------------------
-- Sample data: bookedEquipment
-- -----------------------------------------------------

INSERT INTO bookedEquipment (bookingID, equipmentID) VALUES (0,3);
INSERT INTO bookedEquipment (bookingID, equipmentID) VALUES (0,4);
INSERT INTO bookedEquipment (bookingID, equipmentID) VALUES (1,2);
INSERT INTO bookedEquipment (bookingID, equipmentID) VALUES (4,3);
INSERT INTO bookedEquipment (bookingID, equipmentID) VALUES (4,4);
INSERT INTO bookedEquipment (bookingID, equipmentID) VALUES (4,5);
INSERT INTO bookedEquipment (bookingID, equipmentID) VALUES (2,1);
INSERT INTO bookedEquipment (bookingID, equipmentID) VALUES (3,0);
INSERT INTO bookedEquipment (bookingID, equipmentID) VALUES (6,3);
INSERT INTO bookedEquipment (bookingID, equipmentID) VALUES (7,6);
INSERT INTO bookedEquipment (bookingID, equipmentID) VALUES (5,5);

-- -----------------------------------------------------
-- Sample data: bookedGases
-- -----------------------------------------------------

INSERT INTO bookedGases (bookingID, gasID) VALUES (0,1);
INSERT INTO bookedGases (bookingID, gasID) VALUES (1,3);
INSERT INTO bookedGases (bookingID, gasID) VALUES (4,4);
INSERT INTO bookedGases (bookingID, gasID) VALUES (3,2);
INSERT INTO bookedGases (bookingID, gasID) VALUES (5,2);

-- -----------------------------------------------------
-- Sample data: bookings
-- -----------------------------------------------------

INSERT INTO bookings (userID, bookedStartTime, bookedFinishTime, verified) VALUES (2, TIMESTAMP(DATE(NOW()),'14:00:00'), TIMESTAMP(DATE(NOW()),'15:30:00'),1);
INSERT INTO bookings (userID, bookedStartTime, bookedFinishTime, verified) VALUES (1, '2021-4-16 12:15:00', '2021-4-16 12:30:00', '2');
INSERT INTO bookings (userID, bookedStartTime, bookedFinishTime, verified) VALUES (3, TIMESTAMP(DATE(NOW()),'16:45:00'),TIMESTAMP(DATE(NOW()),'17:30:00'), '1');
INSERT INTO bookings (userID, bookedStartTime, bookedFinishTime, verified) VALUES (2, TIMESTAMP(DATE(NOW()),'10:15:00'), TIMESTAMP(DATE(NOW()),'12:45:00'), '1');
INSERT INTO bookings (userID, bookedStartTime, bookedFinishTime, verified) VALUES (3, TIMESTAMP(DATE(NOW()),'12:15:00'), TIMESTAMP(DATE(NOW()),'12:30:00'), '1');
INSERT INTO bookings (userID, bookedStartTime, bookedFinishTime) VALUES (3, '2021-3-28 16:45:00', '2021-3-28 17:30:00');
INSERT INTO bookings (userID, bookedStartTime, bookedFinishTime) VALUES (3, '2021-5-4 10:15:00', '2021-5-4 12:45:00');
INSERT INTO bookings (userID, bookedStartTime, bookedFinishTime, verified) VALUES (3, TIMESTAMP(DATE(NOW()),'15:30:00'), TIMESTAMP(DATE(NOW()),'15:45:00'),1);

-- -----------------------------------------------------
-- Sample data: Chemicals
-- -----------------------------------------------------

INSERT INTO chemicals (name, quantity, deliveryDate, expiryDate) VALUES("Hydrogen Chloride",69,"2021-01-16","2022-01-16");
INSERT INTO chemicals (name, quantity, deliveryDate, expiryDate) VALUES("Hydrogen Peroxide",1,"2020-02-03","2021-02-05");
INSERT INTO chemicals (name, quantity, deliveryDate, expiryDate) VALUES("Hydrogen Cyanide",8,"2021-03-17","2022-06-21");
INSERT INTO chemicals (name, quantity, deliveryDate, expiryDate) VALUES("Sodium Hypochlorite",0,"2021-01-04","2022-01-26");
INSERT INTO chemicals (name, quantity, deliveryDate, expiryDate) VALUES("Dihydrogen Monoxide",6,"2014-01-01","2024-12-31");

-- -----------------------------------------------------
-- Sample data: Equipment
-- -----------------------------------------------------

INSERT INTO equipment (model, equipmentCategoryID, location, servicePeriod) VALUES ("DFL7340", 1, "Area 3", "Annually");
INSERT INTO equipment (model, equipmentCategoryID, location, servicePeriod) VALUES ("UF3000EX",2,"Area 2", "Annually");
INSERT INTO equipment (model, equipmentCategoryID, location, servicePeriod) VALUES ("UF200R",2,"Area 5", "Annually");
INSERT INTO equipment (model, equipmentCategoryID, location, servicePeriod) VALUES ("PG3000RMX",3,"Area 11", "Annually");
INSERT INTO equipment (model, equipmentCategoryID, location, servicePeriod) VALUES ("300 mm Wafer",4,"Area 2", "Annually");
INSERT INTO equipment (model, equipmentCategoryID, location, servicePeriod) VALUES ("W-GM-5200",5,"Area 9", "Annually");
INSERT INTO equipment (model, equipmentCategoryID, location, servicePeriod) VALUES ("W-GM-4200",5,"Area 5", "Annually");
INSERT INTO equipment (model, equipmentCategoryID, location, servicePeriod) VALUES ("HRG3000RMX",6,"Area 13", "Annually");



-- -----------------------------------------------------
-- Sample data: Equipment Categories
-- -----------------------------------------------------

INSERT INTO  equipmentcategories (name) VALUES ("Dicing Machine");
INSERT INTO  equipmentcategories (name) VALUES ("Probing Machine");
INSERT INTO  equipmentcategories (name) VALUES ("Polish Grinder");
INSERT INTO  equipmentcategories (name) VALUES ("ChaMP System");
INSERT INTO  equipmentcategories (name) VALUES ("Water Manufacturing System");
INSERT INTO  equipmentcategories (name) VALUES ("High Rigid Grinder");

-- -----------------------------------------------------
-- Sample data: Equipment Services
-- -----------------------------------------------------

INSERT INTO equipmentservices (equipmentID, date, engineer) VALUES (0,"2021-03-18","Amelia Myers");
INSERT INTO equipmentservices (equipmentID, date, engineer) VALUES (1,"2014-06-07","Bruce Waller");
INSERT INTO equipmentservices (equipmentID, date, engineer) VALUES (2,"2019-12-04","Casey Bronson");


-- -----------------------------------------------------
-- Sample data: Gases
-- -----------------------------------------------------

INSERT INTO gases (gasID, name, deliveryDate, expiryDate, quantity) VALUES (1,'Carbon monoxide', '2008-7-04', '2010-7-04', 2);
INSERT INTO gases (gasID, name, deliveryDate, expiryDate, quantity) VALUES (2,'Fluorine', '2008-8-10', '2010-8-10', 3);
INSERT INTO gases (gasID, name, deliveryDate, expiryDate, quantity) VALUES (3,'Argon', '2008-9-15', '2010-9-15', 4);
INSERT INTO gases (gasID, name, deliveryDate, expiryDate, quantity) VALUES (4,'Oxygen', '2008-10-20', '2010-10-20', 5);
INSERT INTO gases (gasID, name, deliveryDate, expiryDate, quantity) VALUES (5,'Nitrogen trifluoride', '2008-11-25', '2010-11-25', 6);

-- -----------------------------------------------------
-- Sample data: Equipment Training
-- -----------------------------------------------------

INSERT INTO equipmentTraining (trainingID, equipmentID) VALUES(2, 2);
INSERT INTO equipmentTraining (trainingID, equipmentID) VALUES(4, 2);
INSERT INTO equipmentTraining (trainingID, equipmentID) VALUES(1, 6);
INSERT INTO equipmentTraining (trainingID, equipmentID) VALUES(2, 1);

-- -----------------------------------------------------
-- Sample data: Training
-- -----------------------------------------------------

INSERT INTO training (trainingID, name) VALUES (0, 'Laboratory Safety Awareness');
INSERT INTO training (trainingID, name) VALUES (2, 'Chemical Health and Safety');
INSERT INTO training (trainingID, name) VALUES (3, 'Slips, Trips and Falls');
INSERT INTO training (trainingID, name) VALUES (4, 'Frist Aid');
INSERT INTO training (trainingID, name) VALUES (5, 'COSHH');

-- -----------------------------------------------------
-- Sample data: User Training
-- ------------------------------------------------------*

INSERT INTO userTraining (userID, trainingID) VALUES (2, 4);

-- -----------------------------------------------------
-- Sample data: Users
-- -----------------------------------------------------

INSERT INTO users (firstName, lastName, emailAddress, password, accessLevelID, contactNumber) VALUES ('Peter', 'Franken', 'sampleEmail1@email.com', "b'$2b$12$QwRbeY4M3WIs1Yi/nSjDQOS/I0pMD0w94jLrAOrMrGDktkFbe0OmO'", 1, '07817273940');
INSERT INTO users (firstName, lastName, emailAddress, password, accessLevelID, contactNumber) VALUES ('Carine', 'Schmitt', 'sampleEmail2@email.com', "b'$2b$12$QwRbeY4M3WIs1Yi/nSjDQOS/I0pMD0w94jLrAOrMrGDktkFbe0OmO'", 2, '07928736401');
INSERT INTO users (firstName, lastName, emailAddress, password, accessLevelID, associatedStaff, contactNumber) VALUES ('Carlos', 'Hernndez', 'sampleEmail3@email.com', "b'$2b$12$QwRbeY4M3WIs1Yi/nSjDQOS/I0pMD0w94jLrAOrMrGDktkFbe0OmO'", 3, 2, '07932654839');
INSERT INTO users (firstName, lastName, emailAddress, password, accessLevelID, contactNumber) VALUES ('Philip', 'Cramer', 'sampleEmail4@email.com', "b'$2b$12$QwRbeY4M3WIs1Yi/nSjDQOS/I0pMD0w94jLrAOrMrGDktkFbe0OmO'", 1, '07019428374');
INSERT INTO users (firstName, lastName, emailAddress, password, accessLevelID, contactNumber) VALUES ('Daniel', 'Tonini', 'sampleEmail5@email.com', "b'$2b$12$QwRbeY4M3WIs1Yi/nSjDQOS/I0pMD0w94jLrAOrMrGDktkFbe0OmO'", 2, '07813937485');


-- -----------------------------------------------------
-- Sample data: navOptions
-- -----------------------------------------------------

INSERT INTO navOptions (name, iconFile, link) VALUES ("Home","home.svg","/");
INSERT INTO navOptions (name, iconFile, link) VALUES ("Bookings","calendar1.svg","/bookings");
INSERT INTO navOptions (name, iconFile, link) VALUES ("Pending Requests","sandTimer.svg","/bookings/moderate");
INSERT INTO navOptions (name, iconFile, link) VALUES ("Equipment","equipment.svg","/equipment");
INSERT INTO navOptions (name, iconFile, link) VALUES ("Chemicals","chemicals.svg","/chemicals");
INSERT INTO navOptions (name, iconFile, link) VALUES ("Gases","gases.svg","/gases");
INSERT INTO navOptions (name, iconFile, link) VALUES ("Training","training.svg","/training");
INSERT INTO navOptions (name, iconFile, link) VALUES ("User Management","groupOfPeople.svg","/userManagement");
INSERT INTO navOptions (name, iconFile, link) VALUES ("Logs","reporting.svg","/logs");
INSERT INTO navOptions (name, iconFile, link) VALUES ("Account","person.svg","/account");

INSERT INTO navOptions (name, iconFile, link) VALUES ("My Bookings","calendar2.svg","/myBookings");
INSERT INTO navOptions (name, iconFile, link) VALUES ("New Booking","calendar3.svg","/newBooking/dateTime");



-- -----------------------------------------------------
-- Sample data: navRelations
-- -----------------------------------------------------

INSERT INTO navRelations (navOptionID, accessLevelID) VALUES (1,1);
INSERT INTO navRelations (navOptionID, accessLevelID) VALUES (2,1);
INSERT INTO navRelations (navOptionID, accessLevelID) VALUES (3,1);
INSERT INTO navRelations (navOptionID, accessLevelID) VALUES (4,1);
INSERT INTO navRelations (navOptionID, accessLevelID) VALUES (5,1);
INSERT INTO navRelations (navOptionID, accessLevelID) VALUES (6,1);
INSERT INTO navRelations (navOptionID, accessLevelID) VALUES (7,1);
INSERT INTO navRelations (navOptionID, accessLevelID) VALUES (8,1);
INSERT INTO navRelations (navOptionID, accessLevelID) VALUES (9,1);
INSERT INTO navRelations (navOptionID, accessLevelID) VALUES (10,1);

INSERT INTO navRelations (navOptionID, accessLevelID) VALUES (1,2);
INSERT INTO navRelations (navOptionID, accessLevelID) VALUES (11,2);
INSERT INTO navRelations (navOptionID, accessLevelID) VALUES (12,2);
INSERT INTO navRelations (navOptionID, accessLevelID) VALUES (7,2);
INSERT INTO navRelations (navOptionID, accessLevelID) VALUES (10,2);

SET FOREIGN_KEY_CHECKS=1;

-- -----------------------------------------------------
-- Test Query: Return all users with access level 'student'
-- -----------------------------------------------------
SELECT * FROM userView WHERE accessLevel = 'student';


-- -----------------------------------------------------
-- Test Query: Return the name and location of all equipment
-- -----------------------------------------------------
SELECT name, location FROM equipmentView;


-- -----------------------------------------------------
-- Test Query: Return the navigation options for a technician
-- -----------------------------------------------------
SELECT * FROM accessLevelNavOptions WHERE accessLevelID = 1;


-- -----------------------------------------------------
-- Test Query: Return the userID with these credentials
-- -----------------------------------------------------
SELECT userID FROM users WHERE emailAddress='sampleEmail1@email.com' AND password="b'$2b$12$QwRbeY4M3WIs1Yi/nSjDQOS/I0pMD0w94jLrAOrMrGDktkFbe0OmO'";


-- -----------------------------------------------------
-- Test Query: Return the name of chemicals with a chemicalID of 2,4 and 5
-- -----------------------------------------------------
SELECT name FROM chemicals WHERE chemicalID IN (2,4,5);


-- -----------------------------------------------------
-- Test Query: Return all untrained equipment for user with the name Peter Franken
-- -----------------------------------------------------
SELECT DISTINCT(equipmentID) FROM requiredTrainingForEquipmentByUser WHERE userID=(SELECT userID from userView WHERE firstName='Peter' AND lastName='Franken');


-- -----------------------------------------------------
-- Test Query: Return all bookings that have not been cancelled
-- -----------------------------------------------------
SELECT * FROM bookingView WHERE verified <> 4;
