from flask import Flask, redirect, request,render_template, jsonify, send_from_directory, session, make_response, url_for
from json import dumps
import json
from flask_session import Session
from datetime import timedelta
import datetime
import mysql.connector
import bcrypt
import time

#make sessions store on server-side
#learnt from flask documentation
#accessed 23/03/2021
#https://flask-session.readthedocs.io/en/latest/

app = Flask(__name__)

#initialise server side sessions

app.config['SESSION_PERMANENT'] = True
app.config['SESSION_TYPE'] = 'filesystem'
app.config['PERMANENT_SESSION_LIFETIME'] = timedelta(hours=6)
app.config['SESSION_FILE_THRESHOLD'] = 100

#end of reference

sess = Session()
sess.init_app(app)

def getDatabaseConnection():
    dbConnection = mysql.connector.connect(
      host="127.0.0.1",
      user="root",
      password="comsc",
      database="ics"
    )
    return dbConnection

def executeQuery(query, params=()):
    connection = getDatabaseConnection()
    cursor = connection.cursor(prepared=True)
    cursor.execute(query,params)
    data = cursor.fetchall()
    connection.close()
    return data

def executeStoredProcedureQuery(procedureName, params=()):
    return executeQuery(getStoredProcedure(procedureName,params),params)

def executeStoredProcedureStatement(procedureName, params=()):
    return executeUpdateQuery(getStoredProcedure(procedureName,params),params)

def getStoredProcedure(procedureName, params=()):
    numberOfValues = len(params)
    paramPlaceholders = "("
    for i in range(numberOfValues):
        paramPlaceholders += "?"
        if i < numberOfValues-1:
            paramPlaceholders += ","
    paramPlaceholders += ")"
    return "CALL " + procedureName + paramPlaceholders


def executeUpdateQuery(query, values=()):
    connection = getDatabaseConnection()
    cursor = connection.cursor(prepared=True)
    cursor.execute(query,values)
    connection.commit()

def getAccessLevelName():
    if loggedIn():
        return executeQuery("SELECT name FROM accessLevel al INNER JOIN users u ON u.accessLevelID = al.accessLevelID WHERE u.userID = ?",(session['userID'],))[0][0]

def getAccessLevelID():
    if loggedIn():
        return executeQuery("SELECT accessLevelID FROM users WHERE userID = ?",(session['userID'],))[0][0]

def accessPermited(accessLevels):
    if loggedIn():
        if getAccessLevelName() in accessLevels:
            return True
    return False

def loggedIn():
    if session.get('userID') is None:
        return False
    else:
        return True

def hashPassword(password):
    return str(bcrypt.hashpw(password.encode(),b'$2b$12$QwRbeY4M3WIs1Yi/nSjDQO'))

def getNavOptions():
    accessLevelID = getAccessLevelID()
    return executeQuery("SELECT * FROM accessLevelNavOptions WHERE accessLevelID = ?",(accessLevelID,))

def checkBookingInProgress():
    if (session.get('newRequestStartDateTime') is None) or (session.get('newRequestFinishDateTime') is None):
        return render_template('newBookingDateTime.html', navOptions=getNavOptions())
    else:
        return True

def addLog(userID, event):
    print("log added")
    print(userID)
    print(event)
    executeStoredProcedureStatement("addLog",(int(userID), event))

@app.route('/')
def index():

    if loggedIn():
        userName = executeQuery("SELECT firstName FROM users WHERE userID = ?",(session['userID'],))[0][0]
        if accessPermited(["Staff", "Student"]):
            return render_template('indexStudentStaff.html', navOptions=getNavOptions(), userName = userName)
        elif accessPermited(["Technician"]):
            bookingDataRaw = executeQuery("SELECT * FROM todaysBookingsSummaryView")
            bookingSummary = {}

            for i in bookingDataRaw:
                time = i[3]
                message = i[0] + " required by " + i[1] + " " + i[2]
                if time in bookingSummary.keys():
                    bookingSummary.get(time).append(message)
                else:
                    bookingSummary[time] = [message]

            return render_template('indexTechnician.html', navOptions=getNavOptions(), userName = userName, summaryData=bookingSummary)
    else:
        return render_template('login.html', messageText="You need to be logged in to view this area")

@app.route('/account/login', methods=['GET','POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')
    if request.method == 'POST':

        if request.form.get('username') == "":
            return "Please provide a username"

        if request.form.get('password') == "":
            return "Please provide a password"

        username = request.form.get('username', default = "Error")
        password = hashPassword(request.form.get('password'))
        print(password)
        if executeQuery("SELECT COUNT(*) FROM users WHERE emailAddress=? AND password=?",(username, password))[0][0] == 0:
            return "Username or password is incorrect"

        session['userID'] = executeQuery("SELECT userID FROM users WHERE emailAddress=? AND password=?",(username, password))[0][0]
        addLog(session['userID'],"Login")
        return "success";

@app.route('/account/logout', methods=['GET','POST'])
def logout():
    if request.method == 'GET':
        if not session.get('userID') is None:
            addLog(session['userID'],"Logout")
            session.pop('userID')
        return render_template('login.html', messageText="You have been logged out")


@app.route('/newBooking/dateTime', methods=['GET','POST'])
def newBookingDateTime():
    if request.method == 'GET':
        if accessPermited(["Staff", "Student"]):
            return render_template('newBookingDateTime.html', navOptions=getNavOptions())
        else:
            return render_template('login.html', messageText="Staff or student access required", urlRedirect="/newBooking/dateTime")
    if request.method == 'POST':
        if request.form.get('requestDate') == "":
            return "Please provide a date"

        if request.form.get('requestStartTimeHour') == "" or request.form.get('requestStartTimeMinute') == "":
            return "Please provide a start time"

        if request.form.get('requestFinishTimeHour') == "" or request.form.get('requestFinishTimeMinute') == "":
            return "Please provide a finish time"

        startDateTimeRaw = request.form.get('requestDate') + " " + request.form.get('requestStartTimeHour')+ ":" + request.form.get('requestStartTimeMinute')
        startDateTime = datetime.datetime.strptime(startDateTimeRaw, '%Y-%m-%d %H:%M')

        finishDateTimeRaw = request.form.get('requestDate') + " " + request.form.get('requestFinishTimeHour')+ ":" + request.form.get('requestFinishTimeMinute')
        finishDateTime = datetime.datetime.strptime(finishDateTimeRaw, '%Y-%m-%d %H:%M')

        if datetime.datetime.now() >= startDateTime:
            return "Your request date and time must be in the future"

        if startDateTime >= finishDateTime:
            return "The request finish time must be later than the start time"

        session['newRequestStartDateTime'] = startDateTime
        session['newRequestFinishDateTime'] = finishDateTime

        return "success"

@app.route('/newBooking/equipment', methods=['GET','POST'])
def newBookingEquipment():
    if request.method == 'GET':
        if accessPermited(["Staff", "Student"]):
            if checkBookingInProgress() != True:
                return checkBookingInProgress()
            return render_template('newBookingEquipment.html', navOptions=getNavOptions())
        else:
            return render_template('login.html', messageText="Staff or student access required", urlRedirect="/newBooking/equipment")


@app.route('/newBooking/gasesChemicals', methods=['GET','POST'])
def newBookingGasesChemicals():
    if request.method == 'GET':
        if accessPermited(["Staff", "Student"]):
            if checkBookingInProgress() != True:
                return checkBookingInProgress()
            return render_template('newBookingGasesChemicals.html', navOptions=getNavOptions())
        else:
            return render_template('login.html', messageText="Staff or student access required", urlRedirect="/newBooking/gasesChemicals")


@app.route('/newBooking/summary', methods=['GET','POST'])
def newBookingSummary():
    if request.method == 'GET':
        if accessPermited(["Staff", "Student"]):
            if checkBookingInProgress() != True:
                return checkBookingInProgress()
            bookingTimeInfo = [session['newRequestStartDateTime'].strftime("%d/%m/%y"),session['newRequestStartDateTime'].strftime("%H:%M"),session['newRequestFinishDateTime'].strftime("%H:%M")]

            if session.get('requestEquipment') is None or len(session['requestEquipment']) == 0:
                equipmentList = []
            else:
                convertedEquipment = [str(equipment) for equipment in session['requestEquipment']]
                equipmentString = ",".join(convertedEquipment)
                equipmentList = executeQuery("SELECT * FROM equipmentView WHERE equipmentID IN (" + equipmentString + ")")

            if session.get('requestGas') is None or len(session['requestGas']) == 0 :
                gasesList = []
            else:
                convertedGases = [str(gas) for gas in session['requestGas']]
                gasesString = ",".join(convertedGases)
                gasesList = executeQuery("SELECT name FROM gases WHERE gasID IN (" + gasesString + ")")


            if session.get('requestChemicals') is None or len(session['requestChemicals']) == 0 :
                chemicalList = []
            else:
                convertedChemicals = [str(chemical) for chemical in session['requestChemicals']]
                chemicalsString = ",".join(convertedChemicals)
                chemicalList = executeQuery("SELECT name FROM chemicals WHERE chemicalID IN (" + chemicalsString + ")")


            return render_template('newBookingSummary.html', navOptions=getNavOptions(), equipmentList=equipmentList, chemicalList=chemicalList, gasesList=gasesList, bookingTimeInfo=bookingTimeInfo)
        else:
            return render_template('login.html', messageText="Staff or student access required", urlRedirect="/newBooking/equipment")

    if request.method == 'POST':
        print(session['userID'])
        print(session['newRequestStartDateTime'])
        print(session['newRequestFinishDateTime'])
        print(session['requestEquipment'])

        userID = int(session['userID'])

        if len(session['requestEquipment']) == 0:
            return "You must select at least 1 item of equipment"

        bookingResponse = executeStoredProcedureQuery("createBooking",(userID,session['newRequestStartDateTime'],session['newRequestFinishDateTime'],request.form.get('notes')))[0]
        bookingMessage = bookingResponse[0]
        print("Booking created")

        if bookingMessage == "success":
            bookingID = bookingResponse[1]
            print("Booking ID: " + str(bookingID))
            if not session.get('requestEquipment') is None:
                for i in session['requestEquipment']:
                    print("Adding equipment to booking: " + str(i))
                    addEquipmentResponse = executeStoredProcedureQuery("addEquipmentToBooking",(bookingID, int(i)))[0][0]
                    if addEquipmentResponse != "success":
                        if executeStoredProcedureQuery("deleteBooking",(bookingID,))[0][0] == "success":
                            return addEquipmentResponse
                        else:
                            return "An error occured in the booking creation process. Please try again."

            if not session.get('requestChemicals') is None:
                for i in session['requestChemicals']:
                    print("Adding chemical to booking: " + str(i))
                    addChemicalResponse = executeStoredProcedureQuery("addChemicalToBooking",(bookingID, int(i)))[0][0]
                    if addChemicalResponse != "success":
                        if executeStoredProcedureQuery("deleteBooking",(bookingID,))[0][0] == "success":
                            return addChemicalResponse
                        else:
                            return "An error occured in the booking creation process. Please try again."

            if not session.get('requestGas') is None:
                for i in session['requestGas']:
                    print("Adding gas to booking: " + str(i))
                    addGasResponse = executeStoredProcedureQuery("addGasToBooking",(bookingID, int(i)))[0][0]
                    if addGasResponse != "success":
                        if executeStoredProcedureQuery("deleteBooking",(bookingID,))[0][0] == "success":
                            return addGasResponse
                        else:
                            return "An error occured in the booking creation process. Please try again."


            session.pop('newRequestStartDateTime')
            session.pop('newRequestFinishDateTime')

            if not session.get('requestEquipment') is None:
                session.pop('requestEquipment')
            if not session.get('requestGas') is None:
                session.pop('requestGas')
            if not session.get('requestChemicals') is None:
                session.pop('requestChemicals')

            return "success"

        else:
            return bookingMessage


@app.route('/newBooking/confirmation', methods=['GET','POST'])
def newBookingConfirmation():
    if request.method == 'GET':
        if accessPermited(["Staff", "Student"]):
            return render_template('newBookingConfirmation.html', navOptions=getNavOptions())
        else:
            return render_template('login.html', messageText="Staff or student access required", urlRedirect="/newBooking/equipment")

@app.route('/search/equipment', methods=['POST'])
def getEquipment():
    if request.method == 'POST':
        if request.form.get('search') == "":
            equipment = tuple(executeQuery("SELECT equipmentID, name, category, location FROM equipmentView"))
        else:
            search = request.form.get('search')
            equipment = tuple(executeQuery("CALL searchEquipment(?)",(search,)))
        return make_response(dumps(equipment))

@app.route('/bookings/equipment/unavailability', methods=['GET'])
def getAvailableEquipment():
    if request.method == 'GET':

        equipment = tuple(executeStoredProcedureQuery("getUnavailableEquipment",(session['newRequestStartDateTime'],session['newRequestFinishDateTime'])))
        print(equipment)

        return make_response(dumps(equipment))

@app.route('/bookings/equipment/untrained', methods=['GET'])
def getUntrainedEquipment():
    if request.method == 'GET':

        equipment = tuple(executeQuery("SELECT DISTINCT(equipmentID) FROM requiredTrainingForEquipmentByUser WHERE userID=?",(session['userID'],)))
        print(equipment)

        return make_response(dumps(equipment))


@app.route('/bookings/equipment/untrained/<equipmentID>', methods=['GET'])
def getRequiredtrainingByEquipment(equipmentID):
    if request.method == 'GET':
        training = tuple(executeQuery("SELECT name FROM requiredTrainingForEquipmentByUser WHERE userID=? AND equipmentID=?",(session['userID'],equipmentID)))
        return make_response(dumps(training))

@app.route('/search/gases', methods=['POST'])
def getGases():
    if request.method == 'POST':
        if request.form.get('search') == "":
            gases = tuple(executeQuery("SELECT gasID, name FROM gases"))
        else:
            search = request.form.get('search')
            gases = tuple(executeQuery("CALL searchGases(?)",(search,)))
        return make_response(dumps(gases))

@app.route('/search/chemicals', methods=['POST'])
def getChemicals():
    if request.method == 'POST':
        if request.form.get('search') == "":
            chemicals = tuple(executeQuery("SELECT chemicalID, name FROM chemicals"))
        else:
            search = request.form.get('search')
            chemicals = tuple(executeQuery("CALL searchChemicals(?)",(search,)))
        return make_response(dumps(chemicals))

@app.route('/requests/equipment', methods=['GET','POST'])
def addRequestEquipment():
    if request.method == 'GET':
        if session.get('requestEquipment') is None:
            return make_response(dumps([]))
        return make_response(dumps(session['requestEquipment']))
    if request.method == 'POST':
        if request.form.get('equipmentID') == "":
            return "No equipment was selected"
        else:
            equipmentID = request.form.get('equipmentID')
            if session.get('requestEquipment') is None:
                session['requestEquipment'] = []

            if equipmentID in session['requestEquipment']:
                session['requestEquipment'].remove(equipmentID)
                return 'removed'
            else:
                session['requestEquipment'].append(request.form.get('equipmentID'))
                return "added"

@app.route('/requests/gases', methods=['GET','POST'])
def addRequestGases():
    if request.method == 'GET':
        if session.get('requestGas') is None:
            return make_response(dumps([]))
        return make_response(dumps(session['requestGas']))
    if request.method == 'POST':
        if request.form.get('gasID') == "":
            return "No gas was selected"
        else:
            gasID = request.form.get('gasID')
            if session.get('requestGas') is None:
                session['requestGas'] = []

            if gasID in session['requestGas']:
                session['requestGas'].remove(gasID)
                return 'removed'
            else:
                session['requestGas'].append(request.form.get('gasID'))
                return "added"


@app.route('/requests/chemicals', methods=['GET','POST'])
def addRequestChemicals():
    if request.method == 'GET':
        if session.get('requestChemicals') is None:
            return make_response(dumps([]))
        return make_response(dumps(session['requestChemicals']))
    if request.method == 'POST':
        if request.form.get('chemicalID') == "":
            return "No chemical was selected"
        else:
            chemicalID = request.form.get('chemicalID')
            if session.get('requestChemicals') is None:
                session['requestChemicals'] = []

            if chemicalID in session['requestChemicals']:
                session['requestChemicals'].remove(chemicalID)
                return 'removed'
            else:
                #if request.form.get('quantity') == "":
                #    return "No quantity was provided"
                session['requestChemicals'].append(request.form.get('chemicalID'))
                return "added"

@app.route("/training/add", methods = ['GET','POST'])
def addTraining():
    if accessPermited(["Technician"]):
        if request.method == "POST":
                return request.form.get("trainingID")
        query = """SELECT u.userID, u.firstName, u.lastName,  t.name FROM users u
                LEFT JOIN usertraining ut ON u.userID = ut.userID
                LEFT JOIN training t ON ut.trainingID = t.trainingID
                ORDER BY u.userID;"""

        dataToSend = []
        dataToSendTemp = executeQuery(query)
        for i in range(0,len(dataToSendTemp)):
            tempArray = []
            for j in range(0,3):
                tempArray.append(dataToSendTemp[i][j])
            tempArray.append(" ")
            if tempArray not in dataToSend:
                dataToSend.append(tempArray)
        trainingArray = []
        for i in range(0,len(dataToSendTemp)):
            tempArray = []
            tempArray.append(dataToSendTemp[i][0])
            tempArray.append(dataToSendTemp[i][3])
            trainingArray.append(tempArray)

        for i in range(0,len(dataToSend)):
            for j in range(0, len(trainingArray)):
                print(trainingArray[j])
                if trainingArray[j][0] == dataToSend[i][0]:
                    dataToSend[i][3] = str(dataToSend[i][3]) + str(trainingArray[j][1]) + "\n"


        print(dataToSend)

        return render_template("addUserTraining.html", data = dataToSend, navOptions=getNavOptions())
    else:
        return render_template('login.html',
        messageText="You must be a technician",
        urlRedirect="/training/add")

@app.route("/training/add/<path>", methods = ['GET', 'POST'])
def addTrainingUser(path):
    if accessPermited(["Technician"]):
        if request.method == "POST":
            trainingToAdd = request.form.get("trainingID")
            executeUpdateQuery("INSERT INTO userTraining(userID, trainingID) VALUES (?,?)", (path,trainingToAdd))
            return "success"
        trainingDataToSend = executeQuery("SELECT * FROM training")
        userDataToSend = executeQuery("SELECT * FROM userTrainingView WHERE userID = ?", (path,))
        print(userDataToSend)
        return render_template("userTraining.html",
        trainingData = trainingDataToSend,
        userData = userDataToSend,
        navOptions=getNavOptions())
    else:
        return render_template('login.html',
        messageText="You must be a technician to view this page",
        urlRedirect="/training/add/"+path)

@app.route("/bookings/startstop", methods = ['GET','POST'])
def startStop():
    if loggedIn():
        userID = session['userID']
        dataToSend = executeQuery("SELECT * FROM bookings WHERE userID = ?", (userID,))
        if request.method == "GET":
            return render_template("startstop.html", data = dataToSend, navOptions=getNavOptions())

        try:
            bookingIDarray = [int(x) for x in request.form.get("bookingIDs", default="Error").split(",")]

        except ValueError:
            print("Nothing checked")
            return render_template("startstop.html",
                                   data = dataToSend,
                                   extraData = "Nothing checked!",
                                   navOptions=getNavOptions())

        for idx, val in enumerate(bookingIDarray):
            executeUpdateQuery("UPDATE bookings SET realStartTime = CAST(? as datetime) WHERE bookingID = ?;",
                            values=(request.form.get("startTime"),val))
            executeUpdateQuery("UPDATE bookings SET realFinishTime = CAST(? as datetime) WHERE bookingID = ?;",
                            values=(request.form.get("endTime"),val))

        return render_template("startstop.html",
                        data = dataToSend,
                        extraData = "Data Updated!",
                        navOptions=getNavOptions())
    else:
            return render_template('login.html', messageText="You must be logged in!", urlRedirect="/bookings/startstop")


@app.route("/bookings/moderate/<path>", methods = ['GET','POST'])
def startStopDetails(path = ""):
    if loggedIn() and path != "":
        dataToSend = executeQuery("SELECT * FROM extraBookingDetails WHERE bookingID = ?;", (path,))
        return render_template("moderateDetails.html", data=dataToSend, navOptions=getNavOptions())
    else:
        return render_template('login.html', messageText="No data received/Not logged in", urlRedirect="/bookings/startstop")

@app.route("/bookings/moderate", methods = ['GET','POST'])
def moderateBooking():
    if accessPermited(["Staff", "Technician"]):
        query = """SELECT b.bookingID, c.name, e.name, g.name, b.notes FROM bookings b
                LEFT OUTER JOIN bookedchemicals bc ON bc.bookingID=b.bookingID
                LEFT OUTER JOIN bookedequipment be ON be.bookingID=b.bookingID
                LEFT OUTER JOIN bookedgases bg on bg.bookingID=b.bookingID
                LEFT OUTER JOIN gases g on g.gasID=bg.gasID
                LEFT OUTER JOIN chemicals c ON c.chemicalID=bc.chemicalID
                LEFT OUTER JOIN equipmentView e on e.equipmentID=be.equipmentID
                WHERE verified = 0;"""
        dataToSend = executeQuery(query)
        if request.json != None:
                return jsonify({'redirect': url_for("startStopDetails", path=json.loads(request.json).get("moreDetails"))})
        if request.method == "POST":
            acceptedArray = request.form.get("acceptedIDs").split(",")
            rejectedArray = request.form.get("rejectedIDs").split(",")
            if acceptedArray[0] != "":
                for idx, val in enumerate(acceptedArray):
                        executeUpdateQuery("UPDATE bookings SET verified = 1 WHERE bookingID = ?", (val,))
                        addLog(session['userID'],"Booking " + val + " status changed to approved")
            if rejectedArray[0] != "":
                for idx, val in enumerate(rejectedArray):
                        executeUpdateQuery("UPDATE bookings SET verified = 2 WHERE bookingID = ?", (val,))
                        addLog(session['userID'],"Booking " + val + " status changed to denied")
        return render_template("moderate.html", data=dataToSend, navOptions=getNavOptions())
    else:
        return render_template('login.html', messageText="Staff or Technician access required", urlRedirect="/bookings/moderate")

@app.route("/equipment/reports", methods = ['GET','POST'])
def equipmentReports():
    if accessPermited(["Technician"]):
        dataToSend = executeStoredProcedureQuery("getEquipmentTimes", (0,))
        dataToSend += executeStoredProcedureQuery("getEquipmentTimes", (1,))
        if request.method == "POST":
            if request.form.get("equipmentName") != "undefined":
                query = """UPDATE equipmentView SET lastReset = CURRENT_TIMESTAMP
                        WHERE name = ?;"""
                executeUpdateQuery(query, (request.form.get("equipmentName"),))
        return render_template('equipmentLogs.html', data=dataToSend, navOptions=getNavOptions())
    else:
        return render_template('login.html', messageText="Technician required", urlRedirect="/equipment/reports")


@app.route('/myBookings', methods=['GET','POST'])
def myBookings():
    if request.method == 'GET':
        if accessPermited(["Staff", "Student"]):

            bookings = executeQuery("SELECT * FROM bookingView WHERE userID = ? AND verified <> 4",(session['userID'],))
            bookedEquipment = executeQuery("SELECT * FROM bookedEquipmentView WHERE userID = ?",(session['userID'],))
            bookedChemicals = executeQuery("SELECT * FROM bookedChemicalView WHERE userID = ?",(session['userID'],))
            bookedGases = executeQuery("SELECT * FROM bookedGasView WHERE userID = ?",(session['userID'],))

            return render_template('myBookings.html', navOptions=getNavOptions(), bookings=bookings, bookedEquipment=bookedEquipment, bookedChemicals=bookedChemicals, bookedGases=bookedGases)
        else:
            return render_template('login.html', messageText="Staff or student access required", urlRedirect="/myBookings")

@app.route('/equipment', methods=['GET','POST'])
def searchEquipment():
        if request.method == 'GET':
            if accessPermited(["Technician"]):
                equipment = executeQuery("SELECT equipmentID, name, location FROM equipmentView")
                return render_template('listEquipment.html', navOptions=getNavOptions(), equipment = equipment)
            else:
                return render_template('login.html', messageText="Technician access required", urlRedirect="/equipment")

@app.route('/equipment/details/<equipmentID>', methods=['GET','POST'])
def equipmentDetails(equipmentID):
    if request.method == 'GET':
        if accessPermited(["Technician"]):
            equipmentDetails = executeQuery("SELECT equipmentID, name, model, location, servicePeriod FROM equipmentView WHERE equipmentID=?",(equipmentID,))[0]
            serviceDetails = executeQuery("SELECT DATE_FORMAT(date,'%d/%m/%Y') , engineer FROM equipmentServices WHERE equipmentID=?",(equipmentID))
            requiredTraining = executeQuery("SELECT name FROM equipmentTrainingView WHERE equipmentID=?",(equipmentID))
            return render_template('equipmentDetails.html', navOptions=getNavOptions(), equipmentDetails = equipmentDetails, serviceDetails = serviceDetails, requiredTraining=requiredTraining)
        else:
            return render_template('login.html', messageText="Technician access required", urlRedirect="/equipment/details/" + str(equipmentID))

@app.route('/myBookings/cancel', methods=['POST'])
def myBookingCancel():
    if request.method == 'POST':

        if session.get('userID') is None:
            return "You must be looged in to cancel this booking"

        if request.form.get('bookingID') == "":
            return "No booking ID was provided"

        userID = executeQuery("SELECT userID FROM bookings WHERE bookingID = ?",(request.form.get('bookingID'),))[0][0]

        if userID == session['userID']:
            executeUpdateQuery("UPDATE bookings SET verified=4 WHERE bookingID = ?",(request.form.get('bookingID'),))
            return "success"
        else:
            return "The user logged in did not make this booking. Only the user who created this booking can cancel this booking."

@app.route('/userManagement', methods=['GET','POST'])
def userManagement():
    if accessPermited(["Technician"]):
        if request.method == 'GET':
            users = executeQuery("SELECT * FROM userView")
            return render_template('userManagement.html', navOptions=getNavOptions(), urlRedirect="/userManagement", users=users)
    else:
        return render_template('login.html', messageText= "Technician access required", urlRedirect="/userManagement")

@app.route('/userManagement/add', methods=['GET','POST'])
def userManagementAdd():
    if request.method == 'GET':
        if accessPermited(["Technician"]):
            return render_template('userManagementAdd.html', navOptions=getNavOptions(), urlRedirect="/userManagement/add")
        else:
            return render_template('login.html', messageText= "Technician access required", urlRedirect="/userManagement/add")
    if request.method == 'POST':

        if request.form.get('firstName') == "":
            return "No first name was provided"

        if request.form.get('lastName') == "":
            return "No last name was provided"

        if request.form.get('email') == "":
            return "No email was provided"

        if request.form.get('passowrd') == "":
            return "No password was provided"

        if request.form.get('contactNumber') == "":
            return "No contact number was provided"

        numberNoSpaces = request.form.get('contactNumber').replace(" ", "")

        if not numberNoSpaces.isdigit() or len(numberNoSpaces) != 11:
            return "The contact number provided was invalid"

        if request.form.get('access') == "":
            return "No access level was provided"

        executeUpdateQuery("INSERT INTO users (firstName, lastName, emailAddress, contactNumber, accessLevelID, password) VALUES (?,?,?,?,?,?)",(request.form.get('firstName'), request.form.get('lastName'), request.form.get('email'), numberNoSpaces, request.form.get('access'), hashPassword(request.form.get('password'))))

        return "success"


@app.route('/userManagement/edit/<userID>', methods=['GET','POST'])
def userManagementEdit(userID):
    if request.method == 'GET':
        if accessPermited(["Technician"]):
            user = executeQuery("SELECT userID, firstName, lastName, emailAddress, contactNumber, accessLevelID FROM users WHERE userID = ?",(userID,))[0]
            return render_template('userManagementEdit.html', navOptions=getNavOptions(), urlRedirect="/userManagement/add", user=user)
        else:
            return render_template('login.html', messageText= "Technician access required", urlRedirect="/userManagement/edit/" + userID)
    if request.method == 'POST':

        if request.form.get('firstName') == "":
            return "No first name was provided"

        if request.form.get('lastName') == "":
            return "No last name was provided"

        if request.form.get('email') == "":
            return "No email was provided"

        if request.form.get('contactNumber') == "":
            return "No contact number was provided"

        numberNoSpaces = request.form.get('contactNumber').replace(" ", "")

        if not numberNoSpaces.isdigit() or len(numberNoSpaces) != 11:
            return "The contact number provided was invalid"

        if request.form.get('access') == "":
            return "No access level was provided"

        executeUpdateQuery("UPDATE users SET firstName=?, lastName=?, emailAddress=?, contactNumber=?, accessLevelID=? WHERE userID=?",(request.form.get('firstName'), request.form.get('lastName'), request.form.get('email'), numberNoSpaces, request.form.get('access'),userID))

        return "success"


@app.route('/userManagement/delete/<userID>', methods=['POST'])
def userManagementDelete(userID):
    if request.method == 'POST':

        if executeQuery("SELECT COUNT(*) FROM users WHERE associatedStaff=?",(userID))[0][0] > 0:
            return "A student is associated with this staff member"

        executeUpdateQuery("DELETE FROM users WHERE userID=?",(userID,))
        return "success"
@app.route('/logs', methods=['GET'])
def getLogs():
    if request.method == 'GET':
        if accessPermited(["Technician"]):
            logs = executeQuery("SELECT userName, event, timestamp FROM logView")
            return render_template('logs.html', navOptions=getNavOptions(), urlRedirect="/logs", logs=logs)
        else:
            return render_template('login.html', messageText= "Technician access required", urlRedirect="/userManagement/edit/" + userID)

if __name__ == "__main__":
    app.run(debug=True)
